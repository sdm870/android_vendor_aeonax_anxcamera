const TAG = "[ANX]";
const NativeLog = new NativeFunction(Module.getExportByName(null, '__android_log_write'), 'int', ['int', 'pointer', 'pointer']);
const NativeTAG = Memory.allocUtf8String(TAG);
NativeLog(3, NativeTAG, Memory.allocUtf8String("Enter"));

rpc.exports = {
    init: function (stage, parameters) {
        NativeLog(3, NativeTAG, Memory.allocUtf8String("Init=" + stage));

        Java.perform(function () {
            NativeLog(3, NativeTAG, Memory.allocUtf8String("Perform Start"));
            //#region Initializer block
            var LogClass = Java.use("android.util.Log");
            var TAG_L = "[ANX]";
            LogClass.v(TAG_L, "Log Start");
            var ExcClass = Java.use("java.lang.Exception")
            function tryUse(callback, label) {
                label = label || "TryUse"
                try {
                    callback();
                } catch (error) {
                    LogClass.e(TAG_L, label + "->" + error)
                }
            }

            tryUse(function () {
                const DataItemFeature = Java.use('OooO0O0.OooO0Oo.OooO00o.OooO0O0');
                var dataItemFeature = DataItemFeature.$new();
                LogClass.v(TAG_L, "Device=" + dataItemFeature.OooOooo());
                LogClass.v(TAG_L, "DeviceImpl=" + dataItemFeature._o00o0O0O.value.$className);
            }, "Failed Device Identification");
            tryUse(function () {
                Java.use('com.android.camera.aftersales.AftersalesManager').checkSelf.implementation = function () {
                    LogClass.v(TAG_L, "AftersalesManager.checkSelf==>NOOP");
                }
            }, "AftersalesManager.checkSelf");

            tryUse(function () {
                //Dual Phone Recording
                Java.use('OooO0O0.OooO0Oo.OooO00o.OooO0O0').o00O000o.implementation = function () {
                    LogClass.v(TAG_L, "Dual Phone Recording==>true");
                    return true;
                }
            }, "Dual Phone Recording");

            tryUse(function () {
                Java.use("com.android.camera.statistic.MistatsWrapper").commonKeyTriggerEvent.implementation = function (str, obj, str2) {
                    LogClass.v(TAG_L, "MistatsWrapper.commonKeyTriggerEvent==>NOOP");
                }
                Java.use("com.android.camera.statistic.MistatsWrapper").mistatEvent.implementation = function (str, map) {
                    LogClass.v(TAG_L, "MistatsWrapper.mistatEvent==>NOOP");
                }
                Java.use("com.android.camera.statistic.OneTrackWrapper").init.implementation = function (context) {
                    LogClass.v(TAG_L, "OneTrackWrapper.init==>NOOP");
                }
                Java.use("com.android.camera.statistic.OneTrackWrapper").track.implementation = function (str, map) {
                    LogClass.v(TAG_L, "OneTrackWrapper.track==>NOOP");
                }
                Java.use("com.android.camera2.CameraCapabilities").isSupportWatermark.implementation = function () {
                    LogClass.v(TAG_L, "CameraCapabilities.isSupportWatermark==>true");
                    return true;
                }
            }, "Stats")

            var x = "";
            NativeLog(3, NativeTAG, Memory.allocUtf8String("Perform End"));
        });
    },
    dispose: function () {
        console.log('[dispose]');
    }
};

function ShowToast(message) {
    if (Java.available) {
        var context = Java.use('android.app.ActivityThread').currentApplication().getApplicationContext();

        Java.scheduleOnMainThread(function () {
            var toast = Java.use("android.widget.Toast");
            toast.makeText(context, Java.use("java.lang.String").$new(message), 1).show();
        });
    }
}